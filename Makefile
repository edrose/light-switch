help:
	@echo "This file is just to install and uninstall the script and systemd files"
	@echo "Use systemctl enable/disable/start/stop light-switch to control"

install:
	cp light-switch /usr/bin
	cp light-switch.service /etc/systemd/system
	systemctl daemon-reload

uninstall:
	rm /usr/bin/light-switch
	rm /etc/systemd/system/light-switch.service
	systemctl daemon-reload

reinstall: uninstall install
